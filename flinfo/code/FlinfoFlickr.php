<?php
/**
 * Flinfo
 *
 * Copyright (C) 2006 Florian Straub  (flominator@gmx,net)
 * Copyright (C) 2010 Florian Straub & Lupo (http://commons.wikimedia.org/wiki/User:Lupo)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * http://www.gnu.org/copyleft/gpl.html
 */

require_once ('FlinfoCache.php');
require_once ('FlinfoStatus.php');
require_once ('FlinfoGlobals.php');
require_once ('FlinfoHooks.php');
require_once ('FlinfoData.php');
require_once ('FlinfoIn.php');

require_once ('api/FlinfoFlickrAPI.php');
require_once ('lib/Curly.php');

/**
 * Flickr input handler
 */
class FlinfoFlickr extends FlinfoIn {

	/**
	 * One-hour cache of commons:User:FlickreviewR/bad-authors.
	 * Located in FlinfoData::dataDirectory()
	 */
	const   BAD_AUTHOR_LIST = "badauthors.txt";

	/**
	 * 24h-cache of Flickr Commons participants.
	 */
	const   FLICKR_COMMONS_CACHE = "flickrcommons.txt";

	/**
	 * 24h-cache of Flickr licenses.
	 */
	const   FLICKR_LICENSE_CACHE = "flickrlicenses.txt";
	
	/**
	 * List of US-governmental licenses
	 */
	const	USGOV_USERS = "flickrusgov.txt";

	private $mServer          = null;
	private $mInfo            = null;
	private $mIdDesc          = null;
	private $mId              = null;
	private $mUserId          = null;
	private $mUrl             = null;
	private $mUserName        = null;
	private $mLicenseCache    = null;
	private $mExif            = null;
	private $mExifInfo        = null;
	private $mOriginalLicense = null;
	private $mRawIncludesExif = false;
	private $mUsGovLicenses   = null;
	
	public function __construct ($parameterFileName, $requestParams) {
		$flickrKey = $this->getParamFile ($parameterFileName);
		if ($flickrKey) {
			$flickrKey = trim ($flickrKey);
			$this->mServer = new FlinfoFlickrAPI($flickrKey);
			$this->mLicenseCache = new FlinfoCache (self::FLICKR_LICENSE_CACHE, array ($this, 'loadFlickrLicenses'), 86400);
			FlinfoHooks::register ('flinfoHtmlAfterTextArea', array ($this, 'htmlHook'));
			if (isset ($requestParams['filter']) && strpos ($requestParams['filter'], 'exif') !== false) {
				$this->mRawIncludesExif = true;
			}
		}
	}

	private function extractId ($rawId) {
	    if (preg_match ('/^\d+$/', $rawId)) {
			// All digits: assume a photo id.
			return array ('id' => $rawId, 'uid' => null, 'secret' => null);
		} else if (preg_match ('!^https?://([^.]+\.)?flickr\.com/(x/t/[^/]+/|#/)?photos/([^/]+)/(\d{6,})(/.*)?$!', $rawId, $matches)) {
			// The x/t/00990056 style links are Flickr clicktracking links.
			$id = $matches[4];
			return array ('id' => $id, 'uid' => $matches[3], 'secret' => null);
		} else if (preg_match ('!^https?://([^.]+\.)?static\.?flickr\.com/[^/ ]+/(\d+)_((\d|[a-f])+)[_.].*$!', $rawId, $matches)) {
			// Flickr link to a specific version
			$id = $matches[2];
			return array ('id' => $id, 'uid' => null, 'secret' => $matches[3]);
		} else {
			// Id could not be determined
			return null;
		}
	}

	public function getInfo ($id) {
		if (!$this->mServer) {
			return array ($id, FlinfoStatus::STATUS_INTERNAL_ERROR);
		}
		$this->mIdDesc = $this->extractId ($id);
		if (!$this->mIdDesc) {
			return array ($id, FlinfoStatus::STATUS_INVALID_ID);
		}
		$this->mId = $this->mIdDesc['id'];
		$data = $this->mServer->photos_getInfo ($this->mId);
		if (is_string ($data)) {
			$this->setServerError ($data);
		} else if (is_array ($data)) {
			$this->mInfo = $data['photo'];
			// Add the info about the available image sizes, too.
			$sizes = $this->mServer->photos_getSizes ($this->mId);
			if (is_array($sizes)) $this->mInfo['sizes'] = $sizes;
			// Now check that we got the right image info
			$this->mUserId = $this->mInfo['owner']['nsid'];
			foreach ($this->mInfo['urls']['url'] as $v) {
				if ($v['type'] == 'photopage') {
					$this->mUrl = $v['_content'];
					break;
				}
			}
			if (   $this->mInfo['id'] != $this->mId
		    	||    $this->mIdDesc['uid'] !== null
		       	   && $this->mIdDesc['uid'] != $this->mUserId
		       	   && $this->mIdDesc['uid'] != $this->mInfo['owner']['username']
		           && $this->mIdDesc['uid'] != $this->getPathAlias ($this->mUrl)
		        ||    $this->mIdDesc['secret'] !== null
		           && $this->mIdDesc['secret'] != $this->mInfo['secret']
		           && $this->mIdDesc['secret'] != $this->mInfo['originalsecret']
		       )
		    {
				// These checks are needed because with URL input, people may enter invalid URLs that look like Flickr URLs, but that actually are wrong in subtle ways.
				// For instance, leave off the last two digits in http://www.flickr.com/photos/27302612@N03/3087959051 ... We'll find the ID 30879590, which is valid,
				// but we get back info about a completely different picture from a different user. Hence we check that uid or the secret do match, otherwise we hit
				// such a stupid case.
		   		return array ($this->mId, FlinfoStatus::STATUS_INVALID_ID);
			}
			if (isset ($this->mInfo['title'])) {
				// Flickr returns strange &amp;quot; sequences in titles, which are supposed to be ". So we actually need to unencode
				// here, and in fact decode &amp first.
				$this->mInfo['title'] = str_replace ('&amp;', '&', $this->mInfo['title']);
				$this->mInfo['title'] = str_replace ('&quot;', '"', $this->mInfo['title']);
			}
			if (isset($this->mInfo['description'])) {
				// Here, too, Flickr returns encoded &amp; and &quot; however, I have yet to come across &amp;quot;. Not sure if this
				// then should always become "; I prefer to do it right here:
				$this->mInfo['description'] = str_replace ('&quot;', '"', $this->mInfo['description']);
				$this->mInfo['description'] = str_replace ('&amp;', '&', $this->mInfo['description']);
				// If we had &amp;quot;, we now will have &quot; which in any case is correct.
			}
			return array ($this->mId, FlinfoStatus::STATUS_OK);
		}
		return array ($this->mId, FlinfoStatus::STATUS_SERVER_FAILURE);
	}

	/**
	 * Given Flickr photo page URL, extract the user ID or path alias. Note that for some photos (e.g. ID 30879590), Flickr
	 * reports a photo page URL that contains neither the nsID nor the userName but a path alias, which is elsewhere given
	 * in the photo owner info. (Flickr reports the url http://www.flickr.com/photos/bensutherland/30879590/ for ID 30879590,
	 * but gives only an nsID of 60179301@N00, a real name "Ben Sutherland", and a user name "Ben Sutherland" for the owner.
	 *
	 * @param $url String A URL to a Flickr photo page
	 * @return String the user name or ID as given in that URL
	 */
	private function getPathAlias ($url) {
		if ($url !== null && preg_match ('!^https?://(www\.)?flickr\.com/photos/([^/]+)/(\d{6,})(/.*)?$!', $url, $matches)) {
			return $matches[2];
		}
		return null;
	}

	public function getAccountId () {
		return $this->mUserId;
	}

	/**
	 * Callback for FlinfoCache. Reads the blacklist from the Commons.
	 * @return string Contents of the blacklist
	 */
	public function getBlackList () {
		$url = "https://commons.wikimedia.org/wiki/User:FlickreviewR/bad-authors?action=raw";
		$badList = Curly::getContents ($url, FlinfoGlobals::USER_AGENT);
		if ($badList) {
			// Normalize line endings.
			$badList = str_replace ("\r\n", "\n", $badList);
			// Strip comments. See mediawiki code: https://gerrit.wikimedia.org/r/#/c/42770/10/includes/UploadWizardFlickrBlacklist.php
			// function getBlacklist()
			$badList = preg_replace ('/(^|\s+)#.*$/m', '', $badList); // We do allow trailing comments, which MediaWiki doesn't (yet)
			// If the first line contains the string "[[Commons:Questionable Flickr images]]", assume it's the old format and remove that line
			$badList = preg_replace ('/^.*\[\[Commons:Questionable Flickr images\]\].*\n/', '', $badList);
			preg_match_all ('/\S+/', $badList, $badItems);
			$badList = implode ("\n", $badItems[0]);
		}
		return $badList;
	}

	public function verifyUser () {
		$cache = new FlinfoCache (self::BAD_AUTHOR_LIST, array ($this, 'getBlackList'), 3600); // One hour caching.
		$badList = $cache->get();
		if (!$badList || !$this->mInfo) return true; // No blacklist
		if (preg_match('/^' . preg_quote($this->mUserId, '/') . '$/m', $badList)) {
			return false;
		}
		$pathName = $this->mInfo['owner']['path_alias'];
		if ($pathName) $pathName = trim ($pathName);
		if ($pathName && preg_match('/^' . preg_quote($pathName, '/') . '$/m', $badList)) {
			return false;
		}
		return true;
	}

	/**
	 * Extract information about a Flickr license from its URL.
	 * @param string $url License URL.
	 * @return array (tag, status_code)
	 *    status_code can be
	 *    0 - License is not Commons-compatible, tag is null.
	 *    1 - Commons-compatible license, tag is set to the Commons-template.
	 *    2 - Flickr commons: is Commons-compatible, tag = 'commons'.
	 *    3 - U.S. Government: is Commons-compatible, tag = 'usgov'.
	 *    4 - License is CC PD mark
	 *    If status_code == 2 or 3, later processing will find the correct Commons license templates to use.
	 */
	private function parseLicenseUrl ($url) {
		if (!$url || $url == "") return array (null, 0);
		$tag = self::ccLicenseFromUrl ($url);
		if ($tag) {
			if (strpos($tag,'Flickr-public domain mark') !== false) {
				return array ($tag, 4);
			}
			return array ($tag, 1);
		}
		// Check for Flickr commons and USGov.
		if (preg_match('!^https?://www.flickr.com/commons/usage/$!', $url)) {
			return array ('commons', 2);
		} else if (preg_match('!^http://www.usa.gov/copyright\.[^/?#]+$!', $url)) {
			return array ('usgov', 3);
		}
		return array (null, 0);
	}

	/**
	 * Callback function for FlinfoCache, gets the list of licenses from Flickr, massages it, and returns it serialized.
	 */
	public function loadFlickrLicenses () {
		if (!$this->mServer) return null;
		$data = $this->mServer->photos_licenses_getInfo();
		if (!$data || is_string ($data)) return null;
		// phpFlickr just returns the bare array, without Flickr's wrappers (['licenses']['license'])
		$result = array();
		foreach ($data as $entry) {
			if (isset ($entry['id']) && isset ($entry['name']) && isset ($entry['url'])) {
				list ($tag, $code) = $this->parseLicenseUrl ($entry['url']);
				$result[$entry['id']] = array ('name' => $entry['name'], 'tag' => $tag, 'code' => $code);
			}
		}
		return (count ($result) > 0) ? serialize ($result) : null;
	}

	/**
	 * Simple local cache of the Flickr license info. Should never be accessed
	 * directly; always use getFlickrLicenses() to get the license info.
	 * @var array
	 */
	private static $flickrLicenses = null;

	/**
	 * Get the Flickr license info.
	 * @return array
	 */
	private function getFlickrLicenses () {
		if (self::$flickrLicenses === null) {
			$data = ($this->mLicenseCache ? $this->mLicenseCache->get() : null);
			self::$flickrLicenses = ($data ? unserialize ($data) : array ());
		}
		return self::$flickrLicenses;
	}

	private function findExif ($tags, &$exif) {
		if (!is_array ($tags)) $tags = array ($tags);
		// Split tagspace:tagname
		$keys = array();
		foreach ($tags as $tag) {
			$t = explode (':', $tag, 2);
			if (count($t) === 2) {
				$keys[] = $t;
			} else {
				$keys[] = array (null, $tag);
			}
		}
		foreach ($keys as $key) {
			foreach ($exif as $etag) {
				if ($etag['tag'] == $key[1] && (!$key[0] || strpos ($etag['tagspace'], $key[0]) === 0)) {
					$result = $etag['raw'];
					if (is_string($result)) $result = trim($result);
					if ($result && (!is_string($result) || $result != '')) {
						return $result;
					}
				}
			}
		}
		return false;
	}

	/**
	 * Check the EXIF data of the image, if available, and try to determine a better license and author info.
	 *
	 * @return array (newTags, author), or null if no alternate licensing available.
	 */
	private function examineExif () {
		$fileName = FlinfoData::dataDirectory() . '/exif/' . str_replace ('@', '_', $this->mUserId) . '.txt';
		if (is_file ($fileName)) {
			$data = file_get_contents ($fileName);
			if (!$data) return null;
			$exif = $this->mServer->photos_getExif ($this->mId);
			if (!is_array ($exif)) {
				if (!isset($this->mInfo['sizes']) || count($this->mInfo['sizes']) == 0) return null;
				// We have exiftool: let's use that for getting EXIF data. Useful
				// if Flickr reports back "Permission denied" (e.g., on the images from the
				// official U.S. Air Force Flickr account, such as
				// http://www.flickr.com/photos/usairforce/5681107996 ).
				$url = $this->mInfo['sizes'][count ($this->mInfo['sizes']) - 1];
				if (!isset($url['source'])) return null;
				$exif = null;
				FlinfoHooks::run ('flinfoExiftool', array ($url['source'], &$exif));
			}
			if (!is_array ($exif)) return null;
			$this->mExif = $exif;
			$lines = explode ("\n", str_replace ("\r\n", "\n", $data));
			$licenseOk = false;
			$matchedExifTag = null;
			$matchedString = null;
			foreach ($lines as $line) {
				$line = trim ($line);
				if (empty ($line) || substr ($line, 0, 1) == '#') continue;
				$equalSign = strpos ($line, '=');
				$tags = preg_split ('/\s*,\s*/', trim (substr ($line, 0, $equalSign)));
				$re = trim (substr ($line, $equalSign + 1));
				$endOfRe = strrpos ($re, '>');
				$licenseTags = trim (substr ($re, $endOfRe + 1));
				$re = trim (substr ($re, 0, $endOfRe));
				$tags = array_fill_keys ($tags, true);
				foreach ($exif as $etag) {
					if (isset ($tags[$etag['tag']])) {
						$licenseOk = preg_match ($re, $etag['raw'], $matches);
						if ($licenseOk) {
							$matchedExifTag = $etag;
							$matchedString = $matches[0];
							$tags = empty ($licenseTags) ? null : preg_split ('/\s*,\s*/', $licenseTags);
							break 2;
						}
					}
				}
			}
			if (!$licenseOk) return null;
			$author = null;
			// And try to find the author
			$author = $this->findExif (array ('Artist', 'Author', 'By-line', 'Creator', 'Writer-Editor'), $exif);
			if ($author) {
				// Let's see if we can append a credit
				$position = $this->findExif (array ('XMP:AuthorsPosition', 'By-lineTitle'), $exif);
				$credit = $this->findExif (array ('XMP:Credit', 'Credit'), $exif);
				if ($position && (!$credit || strpos ($position, $credit) === false && strpos ($credit, $position) === false)) {
					$author .= '/' . $position;
				}
				if ($credit) $author .= '/' . $credit;
			}
			return array ($tags, $author, $matchedExifTag, $matchedString);
		}
	}

	public function getLicenses ($goodUser) {
		if (!$this->mInfo) {
			return array ("UNKNOWN FLICKR LICENSE", null, null);
		}
		$tags = array();
		$source = null;
		$license = $this->mInfo['license'];
		$status = 0;
		if ($goodUser) {
			$licenses = $this->getFlickrLicenses();
			if (isset ($licenses[$license])) {
				$status = $licenses[$license]['code'];
			}
			switch ($status) {
				case 1:
				    // Valid Commons-compatible license (cc-by, cc-by-sa, or cc-zero)
					$tags[] = $licenses[$license]['tag'];
					break;
				case 2:
					// No known restrictions...
					list ($local_tags, $src) = $this->getFlickrCommonsLicense ();
					if (!is_array ($local_tags)) {
						$tags[] = $local_tags;
					} else {
						$tags = $local_tags;
					}
					if ($src) $source = $src;
					break;
				case 3:
				    // U.S. government
					$tags[] = $this->getFlickrUSGovLicense ();
					break;
				case 4:
					// CC public domain mark is not good enough for the commons. The tag asks the user to supply a real PD tag.
					// Let's try to help...
					$alternateTag = $this->getKnownFlickrUSGovLicense ();
					if ($alternateTag !== null) {
						$tags[] = $alternateTag;
						$status = 3;
					} else {
						$tags[] = $licenses[$license]['tag'];
						$status = 1; // Try EXIF, too
					}
					break;
				default:
					$status = 0;
			}
			if ($status <= 1) {
				$exifInfo = $this->examineExif();
				if ($exifInfo !== null && $exifInfo[0] !== null) {
					$this->mOriginalLicense = $this->getLicenseName ($license);
					$tags = $exifInfo[0];
					$status = 4;
					$this->mExifInfo = array ('author' => $exifInfo[1], 'exiftag' => $exifInfo[2], 'match' => $exifInfo[3]);
				}
			}
		}
		// Check whether it might come from the NARA.
		if (isset ($this->mInfo['description'])) {
			$nara = $this->naraSource ($this->mInfo['description']);
			if ($nara != null && $nara != "") {
				if ($source) $source .= "\n$nara"; else $source = $nara;
			}
		}
		if ($status == 0) {
			$status = $this->getLicenseName ($license);
		} else {
			$status = null;
			$tags[] = $this->getReviewTag();
		}
		return array ($status, $tags, $source);
	}

	protected function internalGetReviewTag () {
		return "flickrreview";
	}

	/**
	 * Map a license ID to its name.
	 * @param string $license Flickr license ID
	 * @return string License name
	 */
	private function getLicenseName ($license)
	{
		$licenses = $this->getFlickrLicenses();
		if (isset($licenses[$license]) && isset($licenses[$license]['name'])) {
			return $licenses[$license]['name'];
		}
		return "UNKNOWN FLICKR LICENSE";
	}

	private function getKnownFlickrUSGovLicense () {
		if ($this->mUserId) {
			if (!$this->mUsGovLicenses) {
				$this->mUsGovLicenses = $this->loadParamFile (self::USGOV_USERS);
			}
			if (isset ($this->mUsGovLicenses[$this->mUserId])) {
				return $this->mUsGovLicenses[$this->mUserId];
			}
		}
		return null;
	}

	private function getFlickrUSGovLicense () {
		$result = $this->getKnownFlickrUSGovLicense();
		if ($result !== null) {
			return $result;
		}
		return 'PD-USGov';
	}

	/** Translate some known Flickr Commons participants' licenses into more specific ones. */
	private static $knownFlickrCommons = array (
	    "hdl.loc.gov/loc.pnp/fsac" => array ("PD-USGov-FSA", "fsac")
	  , "hdl.loc.gov/loc.pnp/pp.fsac" => array ("PD-USGov-FSA", "fsac")
	  , "hdl.loc.gov/loc.pnp/ggbain" => array ("PD-Bain", "ggbain")
	  , "hdl.loc.gov/loc.pnp/pp.ggbain" => array ("PD-Bain", "ggbain")
	  , "www.powerhousemuseum.com/collection/database/?irn" => array ("PD-Australia", null)
	  , "brooklynmuseum.org" => array (array ("Flickr-Brooklyn-Museum-image", "Flickr-no known copyright restrictions"), null)
	);

	/**
	 * Get the Commons license to use for a particular Flickr Commons participant
	 *
	 * @return array (copyright tags, source link)
	 *     The copyright tags are an array of strings. The source link may be null.
	 */
	private function getFlickrCommonsLicense () {
		$desc = $this->mInfo['description'];
		$source = null;
		$tags = null;
		if ($desc) {
			foreach (self::$knownFlickrCommons as $key => $item) {
				if (stristr ($desc, $key)) {
					$tags = $item[0];
					if ($item[1]) $source = $this->locSource ($desc, $item[1]);
					break;
				}
			}
		}
		if (!$tags) $tags = "Flickr-no known copyright restrictions";
		return array ($tags, $source);
	}

	/**
	 * Construct a LOC-image template.
	 *
	 * @param $desc string The Flickr description
	 * @param $collection string LOC collection identifier (e.g. "ggbain")
	 *
	 * @return LOC-image template in wiki syntax.
	 */
	private function locSource ($desc, $collection)
	{
		$loc_url_prefix   = 'hdl.loc.gov/loc.pnp/'.$collection.'.';
		$start_of_loc_url = strpos ($desc, $loc_url_prefix) + strlen ($loc_url_prefix);
		$end_of_loc_url   = strpos($desc, "\"", $start_of_loc_url);
		$loc_url_len      = $end_of_loc_url - $start_of_loc_url;
		$loc_id           = substr($desc, $start_of_loc_url, $loc_url_len);
		return "{{LOC-image|id=" . $collection . '.' . $loc_id . "}}";
	}

	/**
	 * Construct a NARA-image template.
	 *
	 * @param $desc string The Flickr description
	 *
	 * @return NARA-image template in wiki syntax, or the empty string.
	 */
	private function naraSource ($desc)
	{
		$nara_url_prefix = 'http://arcweb.archives.gov/arc/action/ExternalIdSearch?id=';
		$start_of_id     = strpos ($desc, $nara_url_prefix);
		if ($start_of_id !== false) {
			$start_of_id += strlen ($nara_url_prefix);
			$end_of_id = strpos ($desc, "\"", $start_of_id);
			$id = substr ($desc, $start_of_id, $end_of_id - $start_of_id);
			return "{{NARA|id=$id}}";
		}
		return "";
	}

	public function getAuthor () {
		$userName = $this->mInfo['owner']['realname'];
		if (!$userName || $userName == "") $userName = $this->mInfo['owner']['username'];
		if (!$userName || $userName == "") {
			$userName = "A Flickr user";
		} else {
			$this->mUserName = $userName;
		}
		$userLoc = $this->mInfo['owner']['location'];
		$authorUrl = 'https://www.flickr.com/';
		if ($this->mUserId) $authorUrl .= 'people/' . $this->mUserId;
		$flickrUser = array ($authorUrl, $userName, $userLoc);
		if ($this->mExifInfo && isset ($this->mExifInfo['author']) && $this->mExifInfo['author']) {
			return array ($flickrUser, array (null, $this->mExifInfo['author'], null));
		}
		return array ($flickrUser);
	}

	public function getDate () {
		return $this->convertISODate($this->mInfo['dates']['taken']);
	}

	public function getSource () {
		if (!$this->mUrl) $this->mUrl = $this->getAlternateSource ();
		$title = $this->getTitle();
		if (!$title || $title == "") $title = 'Flickr';
		return array ($this->mUrl, $title);
	}

	public function getAlternateSource () {
		return 'https://www.flickr.com/photos/' . $this->mUserId . '/' . $this->mId;
	}

	/**
	 * List of known Flickr Commons participants. Key is the Flickr photo owner's real name, value
	 * is a URL to their permissions page. Read from Flickr.
	 */
	private static $knownFlickrCommonsPermissions = null;
	private static $flickrAskedForCommonsParticipants = false;

	/**
	 * Callback for FlinfoCache, returning the list of known Flickr Commons participants. The operation tries
	 * to obtain the data from Flickr.
	 */
	public function getCommonsParticipants () {
		if (!$this->mServer) return null;
		self::$flickrAskedForCommonsParticipants = true;
		$data = $this->mServer->commons_getInstitutions ();
		if (!$data || is_string ($data)) return null;
		// Parse the result into our format
		$result = array();
		foreach ($data as $entry) {
			if (isset($entry['name']) && isset($entry['urls']) && is_array($entry['urls']) && isset($entry['urls']['url'])) {
				foreach ($entry['urls']['url'] as $url) {
					if ($url['type'] == 'license') {
						$result[$entry['name']] = $url['_content'];
					}
				}
			}
		}
		return (count ($result) > 0) ? serialize ($result) : null;
	}

	private function getOverridePermission () {
		$result = array ();
		if ($this->mOriginalLicense) {
			$result['license'] = $this->mOriginalLicense;
			if ($this->mExifInfo) {
				if (isset ($this->mExifInfo['exiftag'])) {
					$tag = $this->mExifInfo['exiftag']['tag'];
					$tagspace = $this->mExifInfo['exiftag']['tagspace'];
					$result['tag'] = $tagspace . ':' . $tag;
				}
				if (isset ($this->mExifInfo['match'])) {
					$result['match'] = $this->mExifInfo['match'];
				}
			}
		}
		return $result;
	}

	public function htmlHook (&$htmlText) {
		$override = $this->getOverridePermission ();
		if (isset ($override['license'])) {
			$html = FlinfoHtmlGen::openElement ('div', array ('style' => 'background:pink;'));
			$html .= FlinfoHtmlGen::text(
				str_replace (
					 array ('_LICENSE_', '_PROGRAM_', '_REPO_')
					,array ($override['license'], FlinfoGlobals::$title, 'Flickr')
					,FlinfoData::msg('license_override')
				)
			);
			$html .= ' ';
			$html .= FlinfoHtmlGen::text(FlinfoData::msg('download_original_hint'));
			$html .= FlinfoHtmlGen::close ('div');
			$htmlText .= $html;
		}
		return true;
	}

	public function getPermission () {
		if ($this->mInfo && $this->mInfo['license'] == 7) {
			$cache = null;
			if (self::$knownFlickrCommonsPermissions === null) {
				// Load from file, if it exists
				$cache = new FlinfoCache (self::FLICKR_COMMONS_CACHE, array ($this, 'getCommonsParticipants'), 86400);
				$data = $cache->get();
				self::$knownFlickrCommonsPermissions = ($data ? unserialize ($data) : array ());
			}
			$permission = null;
			if (!isset(self::$knownFlickrCommonsPermissions[$this->mUserName]) && !self::$flickrAskedForCommonsParticipants) {
				// If we didn't load them yet, try again; this time forcing an update from Flickr
				if (!$cache) {
				  $cache = new FlinfoCache (self::FLICKR_COMMONS_CACHE, array ($this, 'getCommonsParticipants'), 86400);
				}
				$data = $cache->get(true);
				self::$knownFlickrCommonsPermissions = ($data ? unserialize ($data) : array ());
			}
			if (!isset(self::$knownFlickrCommonsPermissions[$this->mUserName])) {
				// If still not known, run hook
				FlinfoHooks::run ('flinfoFlickrUnknownCommons', array ($this->mUserName, $this->mId, &$permission));
			} else {
				$permission = self::$knownFlickrCommonsPermissions[$this->mUserName];
			}
			if ($permission) {
				return array ($permission, $this->mUserName . ' @ Flickr Commons');
			}
		}
		$override = $this->getOverridePermission();
		if (isset($override['license'])) {
			$str = FlinfoData::replaceBrackets($override['license']);
			if (isset ($override['tag'])) $str .= '|' . FlinfoData::replaceBrackets($override['tag']);
			if (isset ($override['match'])) $str .= '|' . FlinfoData::replaceBrackets($override['match']);
			return array (null, '{{FlinfoExifLicense|' . $str . '}}');
		}
		return array (null, null);
	}

	public function getDescription () {
		if ($this->mInfo) {
			return trim ($this->mInfo['description']);
		}
		return "";
	}

	public function getTitle () {
		if ($this->mInfo) {
			return trim ($this->mInfo['title']);
		}
		return "";
	}

	public function getCategories () {
		$cats = array();
		if (isset ($this->mInfo['tags']) && isset ($this->mInfo['tags']['tag'])) {
			foreach ($this->mInfo['tags']['tag'] as $tag) {
				$cats[] = strtoupper (substr($tag['raw'], 0, 1)) . substr($tag['raw'], 1);
			}
	  	}
	  	if ($this->mExif) {
	  		// Try to extract location information from EXIF
	  		$loc = $this->findExif ('City', $this->mExif);
	  		if (!$loc) $loc = $this->findExif (array ('State', 'Province', 'Province-State'), $this->mExif);
	  		if (!$loc) $loc = $this->findExif (array ('Country', 'Country-PrimaryLocationName', 'Location'), $this->mExif);
	  		if ($loc && !empty ($loc)) $cats[] = strtoupper (substr($loc, 0, 1)) . substr($loc, 1);
	  	}
		return $cats;
	}

	/**
	 * @see flinfo/FlinfoIn#getGeoInfo()
	 */
	public function getGeoInfo () {
		if (   $this->mInfo
		    && isset ($this->mInfo['location'])
		    && isset ($this->mInfo['location']['latitude'])
		    && isset ($this->mInfo['location']['longitude'])
		   )
		{
			return array ( 'latitude' => "" . $this->mInfo['location']['latitude']
			              ,'longitude' => "" . $this->mInfo['location']['longitude']
			              ,'source' => 'Flickr');
		}
		return null;
	}

	public function getRawResult () {
		if ($this->mRawIncludesExif && $this->mExif) {
			return array ('flickr' => $this->mInfo, 'exif' => $this->mExif);
		}
		return $this->mInfo;
	}

	public function getSizes () {
		if ($this->mInfo && isset ($this->mInfo['sizes'])) {
			return $this->mInfo['sizes'];
		}
		return null;
	}

}
