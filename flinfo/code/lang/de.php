<?php

//Form elements
$messages['subtitle']= '... Bilder einfacher nach Wikimedia Commons hochladen';
$messages['ID'] = 'Foto-ID oder URL:';
$messages['raw'] = 'Nur-Text-Ausgabe';
$messages['get_button'] = 'Info anfordern';

$messages['preview'] = 'Vorschau';
$messages['upload_form'] = 'https://commons.wikimedia.org/w/index.php?title=Special:Upload&uselang=de&uploadformstyle=basic&wpDestFile=';
$messages['preview_form'] = 'https://commons.wikimedia.org/w/index.php?title=Special:ExpandTemplates&uselang=de&wpRemoveComments=1&wpContextTitle=';
$messages['download_org'] = 'Größte Version des Bild herunterladen';
$messages['open_upload_form'] = 'Upload-Formular öffnen';
$messages['pixels'] = 'Pixel';
$messages['repo_label'] = 'Bilddatenbank:';
$messages['repo_hint'] = '(wird ignoriert wenn eine eindeutige URL eingegeben wird)';

$messages['view_at'] = 'Seite bei _REPO_';
$messages['view_exif'] = 'EXIF ansehen';
$messages['view_exif_title'] = "mit Jeffrey's Exif Viewer";

//Textarea
$messages['created'] = "%Y-%m-%d %H:%M";
$messages['copyvio_template'] = 'copyvio';
$messages['speedy_template'] = 'speedy|reason=Foto eines Flickr-Benutzers auf der [[:commons:User:FlickreviewR/bad-authors|schwarzen Liste]]. --~~~~';
$messages['copyvio_reason'] = 'Licensed at the source as "_LICENSE_NAME_" which is no free license.';
$messages['new_commons'] = "Unbekannte Lizenz gefunden. Das bedeutet im Normalfall, dass eine neue Organisation bei Flickrs The Commons aufgenommen wurde. [[Benutzer:Flominator]] wurde gerade hierüber informiert. Bitte in ein paar Tagen nochmal versuchen.";
$messages['uncat'] = '{{subst:unc}}';

//Footer
$messages['manual'] ='Hilfe';
$messages['manual_link'] ='https://de.wikipedia.org/wiki/Benutzer:Flominator/Flinfo';
$messages['contact']='Kontakt';
$messages['contact_link']='https://de.wikipedia.org/wiki/Benutzer_Diskussion:Flominator/Flinfo';
$messages['source']='Quellcode';
$messages['bookmarklet']='Bookmarklet';
$messages['bookmarklet_title'] = "Einfach als Lesezeichen speichern und von einer unterstützten Seite aus aufrufen";

//Errors & Warnings
$messages['error_nonfree'] = 'Bild ist nicht frei lizenziert.';
$messages['error_server']='Keine Information zu Bild _PHOTOID_ gefunden. _FLINFO_SERVER_ERROR_';
$messages['error_internal']='Interner Fehler.';
$messages['error_baduser']='Benutzer steht auf der schwarzen Liste; Bilder dieses Benutzers dürfen nicht nach Wikimedia Commons hochgeladen werden.';
$messages['error_invalid_id']='Bild-ID kann nicht berechnet werden aus der Eingabe _PHOTOID_';
$messages['error_missing_id']='Eine Bild-ID muss angegeben werden.';

$messages['license_override']='_PROGRAM_ verwendet statt der "_LICENSE_" Lizenz auf _REPO_ eine weniger restriktive Lizenz auf Grund der Bildbeschreibung oder der Metadaten des Bildes.';
$messages['download_original_hint'] = 'Bitte unbedingt das Bild in seiner größten Version herunterladen (Download-Link siehe unten) und dann diese nach Wikimedia Commons hochladen.';
$messages['missing_species']='Keine Kategorie auf Commons für "_SPECIES_" gefunden. Bitte überprüfen, wie dieses Bild auf Commons zu kategorisieren ist.';

$messages['already_exists']='Dieses Bild existiert eventuell auf Commons schon:';
