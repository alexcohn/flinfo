<?php
/**
 * Flinfo
 *
 * Copyright (C) 2006 Florian Straub  (flominator@gmx,net)
 * Copyright (C) 2010 Florian Straub & Lupo (http://commons.wikimedia.org/wiki/User:Lupo)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * http://www.gnu.org/copyleft/gpl.html
 */

/**
 * Flinfo input handler for Google's Picasa API. Note that you always need at the very least the user ID and
 * the photo or album ID. Without user ID, no information can be retrieved. Static Picasa photo links from
 * *.ggpht.com/ or from *.googleusercontent.com do not contain that information, and AFAIK there is no known
 * way to calculate the user name and photo ID from such a link.
 *
 * Other kinds of strange Picasa links like search result links or redirect links can be handled, though.
 */
require_once ('api/FlinfoPicasaAPI.php');
require_once ('FlinfoStatus.php');
require_once ('FlinfoIn.php');
require_once ('lib/Curly.php');

require_once ('FlinfoHooks.php');

/**
 * Support class to handle concurrent cURl requests (used for handling search result links).
 */
class FlinfoPicasaRequestHandler implements CurlyRequestHandler {
	public $user = null;
	public $photo = null;
	public $album = null;

	public function __construct ($photoId) {
		$this->photo = $photoId;
	}

	public function processResult ($data, $request) {
		if (!$data) return true; // Empty response
		if ($request) {
			$error = Curly::getError ($request, sizeof($data));
			if ($error) return true; // Some error occurred; just ignore it
		}
		if (preg_match ('!"https?://picasaweb\.google\.[^/]+/data/feed/[^/]+/user/([^/]+)(/album(id)?/([^/]+))?/photoid/' . preg_quote ($this->photo, '!') . '\D!', $data, $matches)) {
			$info = FormatJson::decode ('{"user":"' . $matches[1] . '", "album":"' . (count($matches) > 4 ? $matches[4] : 'null') .'"}', true);
			$this->user = urldecode($info['user']);
			$this->album = urldecode($info['album']);
			return false; // Found it, so stop processing.
		}
		return true;
	}
}

/**
 * Main Picasa input handler.
 */
class FlinfoPicasa extends FlinfoIn {

	private $mServer    = null;
	private $mUrl       = null;
	private $mSizes     = null;
	private $mPhotoInfo = null;
	private $mAlbumInfo = null;
	private $mId        = null;

	public function __construct ($parameterFileName, $requestParams) {
		$this->mServer = new FlinfoPicasaAPI();
	}

	private function getQueryParameters ($query) {
		$params = array ();
		foreach (explode ('&', $query) as $arg) {
			list ($key, $value) = explode ('=', $arg);
			$params[$key] = urldecode ($value);
		}
		return $params;
	}

	private function extractId ($rawId) {
		if (preg_match ('!^https?://plus.google.[^/]+/.*?photos/([^/]+)/albums/(\d+)/(\d+)!', $rawId, $matches)) {
			// Google+ Picasa redirect
			return array ('user' => $matches[1], 'album' => $matches[2], 'photo' => $matches[3]);
		} else if (preg_match ('!^https?://picasaweb.google.[^/]+/lh/view\?[^#]+#(\d+)$!', $rawId, $matches)) {
			// Search. Now this is not nice. We need to do queries until we find the image id. However, this approach is
			// approximative; there is no guarantee that performing a search twice will return the same images (or even
			// roughly the same image set). It works best if the user searched shortly before, and didn't page through the
			// search results much. It appears that the differences in the return set are greater the further back in
			// the original search $rawId turned up.
			//
			// Note that even the Picasa web UI appears to use this method to build the image page if you click on a
			// result thumbnail in the search results. I don't quite understand why (they do have enough information to
			// link to the proper image page directly), but anyway, this whole Picasa stuff is pretty awkward.
			//
			// HTML scrape to find the feed URL.
			$photo = $matches[1];
			$data = Curly::getContents ($rawId);
			if ($data && preg_match ('!\{\s*feedUrl\s*:\s*\'(https?://picasaweb.google.[^/]+/data/feed/[^\']+)\'!', $data, $matches)) {
				// Ok, now get the feed
				$feed = html_entity_decode ($matches[1]);
				$href = $feed . '&max-results=30';
				$handler = new FlinfoPicasaRequestHandler($photo);
				$data = Curly::getContents ($href);
				if ($data && $handler->processResult($data, null)) {
					// Not found yet. Let's parse this properly; get the number of results; then construct requests up to at most
					// 1000 entries, and get them. 1000 seems to be a hard limit of the Picasa API, no matter if we use the tiny,
					// api, or xml feed: the server always responds with error 400, indicating "Too many results requested" in
					// $data as soon as start_index + max_results - 1 > 1000.
					$data = FlinfoPicasaAPI::parse ($data);
					if ($data && isset ($data['feed']) && isset ($data['feed']['totalResults'])&& isset ($data['feed']['entry'])) {
						// Do not use the "next" link(s). First, this would force sequential processing, and second, they do not
						// seem to faithfully preserve all parameters (imglic seems to be dropped, for instance).
						$nofResults = $data['feed']['totalResults'];
						if (is_numeric ($nofResults)) {
							$gotResults = count ($data['feed']['entry']);
							if ($nofResults > 1000) $nofResults = 1000;
							if ($nofResults > $gotResults) {
								$requests = array ();
								$batch = 100;
								for ($start = $gotResults + 1; $start <= $nofResults; $start += $batch) {
									if ($start + $batch > $nofResults + 1) $batch = $nofResults + 1 - $start;
									$requests[] = Curly::getRequest ($feed . '&start-index=' . $start . '&max-results=' . $batch);
								}
								Curly::multiRequest ($requests, $handler);
							}
						}
					}
				}
				if ($handler->user) {
					return array ('user' => $handler->user, 'album' => $handler->album, 'photo' => $photo);
				}
			}
		} else if (preg_match ('!^https?://picasaweb.google.[^/]+/lh/sredir\?([^#]+)(#(\d+))?$!', $rawId, $matches)) {
			// Redirect link. Examine parameters.
			$anchor = (count($matches) > 3) ? urldecode($matches[3]) : null;
			$params = $this->getQueryParameters ($matches[1]);
			$user = (isset($params['uname']) && $params['uname'] && $params['uname'] != '') ? $params['uname'] : null;
			if ($user) {
				$album = null;
				$photo = $anchor;
				if (isset($params['id']) && $params['id'] && $params['id'] != '') {
					if (isset($params['target']) && $params['target'] == 'PHOTO') {
				   		$photo = $params['id'];
					} else if (isset($params['target']) && $params['target'] == 'ALBUM') {
						$album = $params['id'];
					}
				}
				if ($photo) {
					$result = array ('user' => $user, 'album' => $album, 'photo' => $photo);
					if (isset ($params['authkey'])) {
						$result['auth'] = 'authkey=' . rawurlencode ($params['authkey']);
					}
					return $result;
				}
			}
		} else if (preg_match ('!^https?://picasaweb\.google\.[^/]+/lh/photo/([^/?#]*)(\?([^#]+))?!', $rawId, $matches)) {
			// GUPI link. Get the file and search for a feed using the GUPI ("Global user photo id"?) from the URL. This is
			// screen-scraping (or rather, HTML scraping) and may break if Google ever decides to change their implementation of
			// this mechanism.
			$gupi = $matches[1];
			if (!$gupi && count($matches) > 3 && $matches[3]) {
				$params = $this->getQueryParameters ($matches[3]);
				if (isset($params['gupi'])) $gupi = $params['gupi'];
			}
			if ($gupi) {
				$data = Curly::getContents ($rawId);
				if ($data && preg_match ('!"https?://picasaweb\.google\.[^/]+/data/feed/[^/]+/user/([^/]+)/photoid/(\d+)\?([^"]*' . preg_quote ($gupi, '!') . ')"!', $data, $matches)) {
					$result = array ('user' => urldecode(html_entity_decode($matches[1])), 'album' => null, 'photo' => $matches[2]);
					// Adding the gupi is for unlisted albums and their photos: it won't find the album, but will give us
					// information and image links. For normal public albums and photos, it doesn't hurt.
					$result['auth'] = 'gupi=' . rawurlencode ($gupi);
					return $result;
				}
			}
		} else if (preg_match ('!^https?://picasaweb\.google\.[^/]+/([^/]+)/([^/?#]+)(/photo)?(\?([^#]+))?#(\d+)$!', $rawId, $matches)) {
			// Picasa image page. This one's simple.
			$result = array ('user' => urldecode($matches[1]), 'album' => urldecode($matches[2]), 'photo' => $matches[6]);
			if ($matches[5] && $matches[5] != "") {
				$params = $this->getQueryParameters ($matches[5]);
				if (isset ($params['authkey'])) {
					$result['auth'] = 'authkey=' . rawurlencode ($params['authkey']);
				}
			}
			return $result;
		}
		// Other links that we could basically handle?

		// We cannot handle static ggpht/googleusercontent links; their cryptic directories are the result of some one-way
		// hash function; it is practically impossible to calculate the user and photo ID from such a link.
		return null;
	}

	public function getInfo ($id) {
		$this->mId        = $id;
		$this->mPhotoInfo = null;
		$this->mAlbumInfo = null;
		$this->mUrl       = null;
		$this->mSizes     = null;
		if (!$this->mServer) {
			return array ($id, FlinfoStatus::STATUS_INTERNAL_ERROR);
		}
		$this->mIdDesc = $this->extractId ($id);
		if (!$this->mIdDesc) {
			return array ($id, FlinfoStatus::STATUS_INVALID_ID);
		}
		$auth = isset ($this->mIdDesc['auth']) ? $this->mIdDesc['auth'] : null;
		$data = $this->mServer->getPhotoInfo($this->mIdDesc['photo'], $this->mIdDesc['user'], $auth);
		if (is_string ($data) || !isset ($data['feed'])) {
			$this->setServerError ($data);
			return array ($id, FlinfoStatus::STATUS_SERVER_FAILURE);
		}
		$this->mPhotoInfo = $data['feed'];
		// Also get album and thumbnail info, and merge it together
		$data = $this->mServer->getThumbnails($this->mIdDesc['photo'], $this->mIdDesc['user'], $auth);
		if (!is_string ($data) && isset($data['feed']) && isset($data['feed']['media$group'])) {
			$data = $data['feed']['media$group'];
			// We should have gotten back an array with a field media$group, containing an object in media$content and
			// a list in media$thumbnail.
			if (!isset ($this->mPhotoInfo['media$group'])) {
				$this->mPhotoInfo['media$group'] = $data;
			} else {
				if (isset ($data['media$content'])) {
					$this->mPhotoInfo['media$group']['media$content'] = $data['media$content'];
				}
				if (isset ($data['media$thumbnail'])) {
					$this->mPhotoInfo['media$group']['media$thumbnail'] = $data['media$thumbnail'];
				}
			}
		}
		$data = $this->mServer->getAlbumInfo ($this->mPhotoInfo['gphoto$albumid'], $this->mIdDesc['user'], $auth);
		if (!is_string ($data) && isset($data['feed'])) {
			$this->mAlbumInfo = $data['feed'];
		}
		list ($url, $title) = $this->getSource();
		return array ($url, FlinfoStatus::STATUS_OK);
	}

	public function getAccountId () {
		if ($this->mIdDesc && isset ($this->mIdDesc['user'])) {
			return $this->mIdDesc['user'];
		}
		return null;
	}

	private function getLicenseName ($license) {
		if ($license && isset ($license['name']) && $license['name'] !== null && $license['name'] != "") {
			return $license['name'];
		}
		return "UNKNOWN PICASA LICENSE";
	}

	public function getLicenses ($goodUser)	{
		$tags = array();
		$source = null;
		$license = $this->mPhotoInfo['gphoto$license'];
		if ($goodUser && $license && isset ($license['url'])) {
			$tag = self::ccLicenseFromUrl ($license['url']);
			if ($tag) $tags[] = $tag;
		}
		$status = null;
		if (count ($tags) == 0) {
			$status = $this->getLicenseName ($license);
		} else {
			$tags[] = $this->getReviewTag();
		}
		return array ($status, $tags, $source);
	}

	protected function internalGetReviewTag () {
		return "picasareview";
	}

	public function getAuthor () {
		$userName = null;
		$authorUrl = null;
		if (isset ($this->mPhotoInfo['author']) && count ($this->mPhotoInfo['author']) > 0) {
			$userName = $this->mPhotoInfo['author'][0]['gphoto$nickname'];
			if (!$userName || $userName == "") $userName = $this->mPhotoInfo['author'][0]['name'];
			$authorUrl = $this->mPhotoInfo['author'][0]['uri'];
		}
		if (!$userName || $userName == "") {
			$userName = "A Picasa user";
		}
		if (!$authorUrl || $authorUrl == "") {
			$authorUrl = 'https://picasaweb.google.com/' . $this->mIdDesc['user'];
		}
		return array (array ($authorUrl, $userName, null));
	}

	public function getDate () {
		// We have two timestamps: exif$tags:exif$time and gphoto$timestamp. Google says they should be equal
		// if the uploaded picture had an EXIF time set, however, I've occasionally seen differences.
		// If the EXIF time is smaller, use that.
		$gtime = $this->mPhotoInfo['gphoto$timestamp'];
		$etime = null;
		if (isset ($this->mPhotoInfo['exif$tags']) && isset ($this->mPhotoInfo['exif$tags']['exif$time'])) {
			$etime = $this->mPhotoInfo['exif$tags']['exif$time'];
		}
		if (!is_numeric($gtime)) {
			$time = $etime;
		} else if (!is_numeric ($etime)) {
			$time = $gtime;
		} else {
			$time = (intval($etime, 10) < intval ($gtime, 10)) ? $etime : $gtime;
		}
		return is_numeric ($time) ? ($time / 1000) : null; // Google reports milliseconds?!
	}

	private function getLink ($links, $rel) {
		foreach ($links as $l) {
			if ($l['rel'] == $rel) return $l['href'];
		}
		return null;
	}

	public function getSource () {
		if ($this->mUrl) return $this->mUrl;
		// Try to construct a nice URL instead of the cryptic redirect URL that Picasa gives us.
		$url = null;
		if ($this->mAlbumInfo) {
			if (isset($this->mAlbumInfo['link'])) {
				$url = $this->getLink ($this->mAlbumInfo['link'], 'alternate');
			} else if (isset($this->mAlbumInfo['gphoto$name'])) {
				$url = 'https://picasaweb.google.com/' . $this->mIdDesc['user'] . '/' . $this->mAlbumInfo['gphoto$name'];
			}
			if ($url !== null) $url .= '#' . $this->mIdDesc['photo'];
		} else if (isset($this->mPhotoInfo['link'])) {
			// This is a cryptic redirect URL.
			$url = $this->getLink ($this->mPhotoInfo['link'], 'alternate');
		}
		if ($url === null) $url = $this->mId; // Still none? Try the input URL.
		$title = $this->getTitle();
		if (!$title || $title == "") $title = 'Picasa';
		$this->mUrl = array ($url, $title);
		return $this->mUrl;
	}

	public function getAlternateSource () {
		$url = null;
		if ($this->mAlbumInfo) {
			if (isset($this->mAlbumInfo['gphoto$name'])) {
				$url = 'https://picasaweb.google.com/' . $this->mIdDesc['user'] . '/' . $this->mAlbumInfo['gphoto$name'];
			}
			if ($url !== null) $url .= '#' . $this->mIdDesc['photo'];
		} else if (isset($this->mPhotoInfo['link'])) {
			// This is a cryptic redirect URL.
			$url = $this->getLink ($this->mPhotoInfo['link'], 'alternate');
		}
		return $url;
	}

	public function getRawResult () {
		return array ('photo' => $this->mPhotoInfo, 'album' => $this->mAlbumInfo);
	}

	public function getDescription () {
		if ($this->mPhotoInfo) {
			$desc = null;
			if (isset ($this->mPhotoInfo['media$group']) && isset ($this->mPhotoInfo['media$group']['media$description'])) {
				$desc = $this->mPhotoInfo['media$group']['media$description'];
			}
			if ((!$desc || $desc == "") && isset ($this->mPhotoInfo['gphoto$htmlCaption'])) {
				$desc = $this->mPhotoInfo['gphoto$htmlCaption'];
			}
			if (!is_string ($desc)) $desc = "";
			return $desc;
		}
		return "";
	}

	private function getGeo ($from, $source) {
		if (   isset ($from['georss$where'])
		    && isset ($from['georss$where']['gml$Point'])
		    && isset ($from['georss$where']['gml$Point']['gml$pos'])
		   ) {
		    $coords = explode(' ', $from['georss$where']['gml$Point']['gml$pos']);
		    if (count($coords) === 2) {
		    	return array ( 'latitude' => $coords[0], 'longitude' => $coords[1], 'source' => $source);
		    }
		}
		return null;
	}

	public function getGeoInfo () {
		$result = null;
		if ($this->mPhotoInfo) {
			$result = $this->getGeo($this->mPhotoInfo, 'Picasa');
		}
		if ($result === null && $this->mAlbumInfo) {
			$result = $this->getGeo($this->mAlbumInfo, 'Picasa_album'); // No blanks in source!
		}
		return $result;
	}

	public function getTitle () {
		if ($this->mPhotoInfo) {
			return $this->mPhotoInfo['title'];
		}
		return "";
	}

	public function getSizes () {
		if ($this->mSizes) return $this->mSizes;
		if (!$this->mPhotoInfo || !isset ($this->mPhotoInfo['media$group'])) {
			return null;
		}
		$result = array ();
		if (isset ($this->mPhotoInfo['media$group']['media$thumbnail'])) {
			 $sizes = $this->mPhotoInfo['media$group']['media$thumbnail'];
			// Format into *our* format: 'width', 'height', 'source'
			foreach ($sizes as $thumb) {
				$result[] = array ('width' => $thumb['width'], 'height' => $thumb['height'], 'source' => $thumb['url']);
			}
		}
		if (isset ($this->mPhotoInfo['media$group']['media$content'])) {
			$original = $this->mPhotoInfo['media$group']['media$content'][0];
			// If we don't have that size already, add it at the end. It should point to the original size.
			// If we _do_ have it, just replace the source URL: we've had cases where the original had more
			// complete EXIF data than the equal-sized thumbnail.
			$w = $original['width'];
			$h = $original['height'];
			$found = false;
			foreach ($result as $key => $thumb) {
				if ($thumb['width'] == $w && $thumb['height'] == $h) {
					$found = true;
					$result[$key]['source'] = $original['url'];
					break;
				}
			}
			if (!$found) {
				$result[] = array ('width' => $w, 'height' => $h, 'source' => $original['url']);
			}
		}
		$this->mSizes = $result;
		return $result;
	}

	public function getCategories () {
		$result = array();
		if (   $this->mPhotoInfo
		    && isset ($this->mPhotoInfo['media$group'])
		    && isset ($this->mPhotoInfo['media$group']['media$keywords'])
		    && is_string ($this->mPhotoInfo['media$group']['media$keywords'])
		   ) {
			$tags = explode (',', $this->mPhotoInfo['media$group']['media$keywords']);
			foreach ($tags as $tag) {
				$cat = trim($tag);
				$result[] = strtoupper (substr($cat, 0, 1)) . substr($cat, 1);
			}
		}
		if ( $this->mPhotoInfo && isset($this->mPhotoInfo['gphoto$location'])) {
			$cat = trim($this->mPhotoInfo['gphoto$location']);
			$result[] = strtoupper (substr($cat, 0, 1)) . substr($cat, 1);
		}
		if ( $this->mAlbumInfo && isset($this->mAlbumInfo['gphoto$location'])) {
			$cat = trim($this->mAlbumInfo['gphoto$location']);
			$result[] = strtoupper (substr($cat, 0, 1)) . substr($cat, 1);
		}
		return $result;
	}

}
